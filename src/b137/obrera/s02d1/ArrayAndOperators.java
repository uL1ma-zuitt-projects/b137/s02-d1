package b137.obrera.s02d1;

public class ArrayAndOperators {

    public static void main(String[] args) {
        System.out.println("Array and Operators\n");

        // Declare an array of integer
        int[] arrayOfNumbers = new int[5];

        System.out.println("The element at index 0 is: " + arrayOfNumbers[0]);
        System.out.println("The element at index 1 is: " + arrayOfNumbers[1]);
        System.out.println("The element at index 2 is: " + arrayOfNumbers[2]);
        System.out.println("The element at index 3 is: " + arrayOfNumbers[3]);
        System.out.println("The element at index 4 is: " + arrayOfNumbers[4]);

        System.out.println();

        // Mini-activity

        arrayOfNumbers[0] = 30;
        arrayOfNumbers[1] = 11;
        arrayOfNumbers[2] = 23;
        arrayOfNumbers[3] = 35;
        arrayOfNumbers[4] = 0;

        System.out.println("The element at index 0 is: " + arrayOfNumbers[0]);
        System.out.println("The element at index 1 is: " + arrayOfNumbers[1]);
        System.out.println("The element at index 2 is: " + arrayOfNumbers[2]);
        System.out.println("The element at index 3 is: " + arrayOfNumbers[3]);
        System.out.println("The element at index 4 is: " + arrayOfNumbers[4]);

        System.out.println();

        String[] arrayOfNames = {
                "Curry", "Thompson", "Green", "Durant", "Lillard"
        };

        System.out.println("#30: " + arrayOfNames[0]);

        System.out.println();

        // Declare an array of arbitrary length
        int[] arrayOfArbitraryNumbers = {};
        //int[] arrayOfArbitraryNumbers;

        // This will create an error
        // arrayOfArbitraryNumbers[0] = 100;
        // ArrayList is the solution
    }
}
